package com.lottostarlet.mint.war.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/api/api/test").permitAll();

		http.exceptionHandling().authenticationEntryPoint(new MintAuthEntryPoint());

		System.out.println("Disabling csrf protection");
		http.csrf().disable();
		http.sessionManagement().sessionFixation().migrateSession();
	}


	@Bean
	public PasswordEncoder passwordEncoder() {
		return new Pbkdf2PasswordEncoder();
	}

	@Bean
	public SessionRegistry sessionRegistry() { return new SessionRegistryImpl(); }

	private static class MintAuthEntryPoint implements AuthenticationEntryPoint {

		@Override
		public void commence(HttpServletRequest request,
				HttpServletResponse response, AuthenticationException exception)
				throws IOException, ServletException {
			System.out.println("Commencing authentication after " + request.getMethod() + " " + request.getRequestURI());
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
		}

	}

}
