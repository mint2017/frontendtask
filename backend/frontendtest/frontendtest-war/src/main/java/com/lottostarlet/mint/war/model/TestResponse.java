package com.lottostarlet.mint.war.model;

import java.io.Serializable;

public class TestResponse implements Serializable{

    private String value = "Congrats. You've managed to do the api call!";

    public TestResponse() {
    }

    public String getValue() {
        return value;
    }
}
