package com.lottostarlet.mint.war.api;

import com.lottostarlet.mint.war.model.TestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = TestController.PATH)
public class TestController {

	public static final String PATH = "/api/test";

	@GetMapping
	public TestResponse testCall() {
		return new TestResponse();
	}
}
