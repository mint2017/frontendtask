# Topbar Classes

These are the classe to add when user actions happen.

##### Transform topbar when user scrolls page
To transform the topbar when the user scrolls the page just add the class `nav-transform` to the `topbar-wrap` container.

##### Open mobile nav when clicked on hamburger icon
To open the mobile navigation & transform the hamburger icon to a `X` add the class `mobile-nav-isActive` to the `topbar-wrap` container.

##### Logged in state: Open subnav when user clicks or hovers over the user icon (desktop)
To open the subnav when the user hovers or clicks on the user icon just overwrite the css property `display: none;` to `display: block;` from the class `subnav-user-account`.

##### Logged in state: Open subnav when user clicks on the menue item "My Account" (mobile)
To open the subnav when the user clicks on the menue item "My Account" just overwrite the css property `display: none;` to `display: block;` from the class `subnav-my-account`.
